<?php
require_once('functions.php');
checkLogin();

//initialize variables
$nameEdit = "";
$mobile = "";
$mail = "";
$newPos = "";
$position_idEdit = 0;
$id = 0;
$edit_state = false;


//click on save button
if (isset($_POST['save'])) {

    $newPos = $_POST['newPos'];

    if ($newPos) {
        $data = validateStaffEntry($_POST);
        global $db;
        $query1 = "INSERT INTO staff_information (name, mobile, email) 
VALUES('{$data['name']}', '{$data['mobile']}', '{$data['email']}')";
        $query2 = "INSERT INTO positions (position) 
VALUES('{$data['newPos']}')";


        if (mysqli_query($db, $query1) && mysqli_query($db, $query2)) {

            $_SESSION['msg'] = "Данните са въведени успешно!";
        } else {

            $_SESSION['msg'] = "Данните НЕ са въведени успешно!";
        }

        redirectToIndex();
    } else {
        $data = validateStaffEntry($_POST);

        $query = "INSERT INTO staff_information (name, mobile, email, position_id) 
VALUES('{$data['name']}', '{$data['mobile']}', '{$data['email']}', '{$data['position_id']}')";


        global $db;
        if (mysqli_query($db, $query)) {
            $_SESSION['msg'] = "Данните са въведени успешно!";
        } else {

            $_SESSION['msg'] = "Данните НЕ са въведени успешно!";
        }

        redirectToIndex();
    }
}


//update records
if (isset($_POST['update'])) {

    $validatedData = validateStaffEntry($_POST);
    global $db;

    $id = intval($_POST['id']);
    $query = 'UPDATE staff_information 
                SET name = "' . $validatedData['name'] . '", '
        . 'mobile = "' . $validatedData['mobile'] . '", '
        . 'email = "' . $validatedData['email'] . '", '
        . 'position_id = ' . $validatedData['position_id']
        . ' WHERE id = ' . $id;

    $updateresult = mysqli_query($db, $query);


    //check for errors

    if ($updateresult) {
        $_SESSION['msg'] = "Записът е променен!";
    } else  $_SESSION['msg'] = "Записът НЕ E променен!";
    redirectToIndex();
}


//delete records
if (isset($_GET['del'])) {
    $id = $_GET['del'];

    if (deleteStaffRecords($_GET['del'])) {
        $_SESSION['msg'] = "Записът е изтрит!";
    } else {
        $_SESSION['msg'] = "Записът НЕ е изтрит!";
    }

    redirectToIndex();
}



