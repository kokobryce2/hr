<?php

require_once('functions.php');
include('server.php');
checkLogin();
global $db;

//fetch the record to be updated
if (isset($_GET['edit'])) {
    $id = intval($_GET['edit']);
    $edit_state = true;
    $rec = mysqli_query($db, "SELECT * FROM staff_information WHERE id=$id");
    $record = mysqli_fetch_array($rec);
    $nameEdit = $record['name'];  //changed variable name here, in form and server.php after search function added
    $mobile = $record['mobile'];
    $mail = $record['email'];
    $position_idEdit = $record['position_id'];  //changed variable name here, in form and server.php after search function added
    $id = $record['id'];

}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Staff statistics</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<a href="logout.php">Logout</a>


<?php
if (isset($_SESSION['msg'])) { ?>
    <div class="msg">
        <?php
        echo $_SESSION['msg'];
        unset ($_SESSION['msg']);
        ?>
    </div>

<?php } ?>


<div class="h1">
    Списък служители
</div>

<!-- SEARCH -->



    <form method="get" action="index.php" class="h2">
        <label>ТЪРСЕНЕ</label>
        <br>
        <label>По име </label><input type="text" name="name" placeholder="Име">

        <label>И / Или </label> <select name="position_id">
            <option value="">Позиция</option>

            <?php

            foreach (getPositions() as $position) {
                $selected = '';

                if ($_GET['position_id'] == $position['id']) {
                    $selected = 'selected="selected"';
                }

                echo '<option  value="' . $position['id'] . '" ' . $selected . '>' . $position['position'] . '</option>';
            }

            ?>

        </select>
        <input type="submit" name="submit" value="Търси" class="btn">
        <a href="index.php">Изчисти</a>
    </form>


<table>
    <thead>
    <tr>
        <th>Име</th>
        <th>Мобилен телефон</th>
        <th>Email</th>
        <th>Длъжност</th>
        <th colspan="2">Действие</th>
    </tr>

    </thead>
    <tbody>
    <!-- if isset searchThing > else foreach -->
    <?php
    $name = null;
    $position_id = null;

    if (isset($_GET['name'])) {
        $name = $_GET['name'];
    }

    if (isset($_GET['position_id'])) {
        $position_id = $_GET['position_id'];
    }

    $data = getStaffRecords($name, $position_id);



    if (count($data)) {
        foreach ($data as $row) {


            ?>
            <tr>
                <td><?php echo $row['name']; ?></td>
                <td><?php echo $row['mobile']; ?></td>
                <td><?php echo $row['email']; ?></td>
                <td><?php echo $row['position']; ?></td>

                <td>
                    <a class="edit_btn" href="index.php?edit=<?php echo $row['id']; ?>">Edit</a>
                </td>
                <td>
                    <a class="del_btn" href="server.php?del=<?php echo $row['id']; ?>">Delete</a>
                </td>
            </tr>
            <?php
        }
    } else {
        echo "<tr><td colspan='6'>Няма намерени резултати!</td></tr>";
    }


    ?>


    </tbody>
</table>
<form method="post" action="server.php">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <div class="input-group">
        <label>Име</label>
        <input type="text" name="name" value="<?php echo $nameEdit; ?>">
    </div>
    <div class="input-group">
        <label>Мобилен телефон</label>
        <input type="tel" name="mobile" value="<?php echo $mobile; ?>">
    </div>
    <div class="input-group">
        <label>Email</label>
        <input type="text" name="email" value="<?php echo $mail; ?>">

        <div class="input-group">
            <label>Длъжност</label>
            <br>

            Изберете: <select name="positiC:\xampp\htdocs\filterDataTable\index.phpon_id">


                <?php

                foreach (getPositions() as $positionEdit) {
                    $selected = '';


                    if ($position_idEdit == $positionEdit['id']) {
                        $selected = 'selected="selected"';
                    }

                    echo '<option  value="' . $positionEdit['id'] . '" ' . $selected . '>' . $positionEdit['position'] . '</option>';
                }


                ?>


            </select>
            <label>или въведете нова: </label><input type="text" name="newPos" placeholder="Длъжност...">

        </div>

        <div class="input-group">
            <?php if ($edit_state == false): ?>
                <button type="submit" name="save" class="btn">Save</button>
            <?php else: ?>
                <button type="submit" name="update" class="btn">Update</button>
            <?php endif ?>
        </div>
    </div>
</form>


</body>
</html>