<?php
session_start();

//connect to database
$db = mysqli_connect('localhost', 'root', '', 'staff');
mysqli_set_charset($db, 'utf8');
if (!$db) {
    die("Connection failed!");
}

function checkLogin()
{
    if (!isLogged()) {
        redirectToLogin();
    }
}

function isLogged()
{
    return array_key_exists('is logged', $_SESSION);
}

function redirectToIndex()
{
    header('Location: index.php');

}

function redirectToLogin()
{
    header('Location: login.php');

}

function deleteStaffRecords($id)
{
    global $db;
    $id = intval($id);
    return mysqli_query($db, "DELETE FROM staff_information WHERE id=$id");
}


//dropdown list
function getPositions()
{
    global $db;

    $sql = "SELECT id, position FROM positions ORDER BY position";
    $result = mysqli_query($db, $sql);
    if (!$result) {
        die("Connection failed: " . mysqli_connect_error());
    }

    return mysqli_fetch_all($result, MYSQLI_ASSOC);
}


function getStaffRecords($filterByName = null, $filterByPositionId = null)
{
    global $db;

    $sql = "
        SELECT
            s.*,
            p.position
        FROM staff_information s
        LEFT JOIN positions p ON p.id = s.position_id
    ";


    if ($filterByPositionId || $filterByName) {

        $sql .= " WHERE ";

        if ($filterByPositionId) {
            $filterByPositionId = intval($filterByPositionId);
            $sql .= " s.position_id = $filterByPositionId ";
        }

        if ($filterByName && $filterByPositionId) {
            $sql .= " AND ";
        }

        if ($filterByName) {
            $filterByName = trim(mysqli_real_escape_string($db, $filterByName));
            $sql .= " s.name LIKE '%$filterByName%' ";
        }

    }

    $result = mysqli_query($db, $sql);


    return mysqli_fetch_all($result, MYSQLI_ASSOC);


}


function validateStaffEntry($inputData)
{
    global $db;
    $staffData = [];

    $name = trim($inputData['name']);
    if (mb_strlen($name) < 4) {
        $_SESSION['msg'] = "Потребителското име трябва да е поне 4 символа.";
        redirectToIndex();
    }
    $name = mysqli_real_escape_string($db, $name);


    $mobile = trim($inputData['mobile']);

    if (!preg_match('/^[0][0-9]{3}[0-9]{3}[0-9]{3}$/', $mobile)) {
        $_SESSION['msg'] = "Моля, въведете валиден телефонен номер (формат: 0*********)";
        redirectToIndex();
    }
    $mobile = mysqli_real_escape_string($db, $inputData['mobile']);


    $email = ($inputData['email']);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $_SESSION['msg'] = "Моля, въведете валиден e-mail";
        redirectToIndex();
    }
    $mail = mysqli_real_escape_string($db, $inputData['email']);


    $position_id = intval($inputData['position_id']);

    $newPos = mysqli_real_escape_string($db, $inputData['newPos']);

    $staffData['name'] = $name;
    $staffData['mobile'] = $mobile;
    $staffData['email'] = $mail;
    $staffData['position_id'] = $position_id;
    $staffData['newPos'] = $newPos;

    return $staffData;
}

/*function newPosition()
{
    global $db;
    $sql = ("INSERT INTO `positions`(`position`) VALUES ({$_POST['newpos']})");

    mysqli_query($db, $sql);
} */